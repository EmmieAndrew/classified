<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Category;
use App\Models\Post;
use App\Models\User;

use Validator;
use Illuminate\Support\Facades\Hash;
use DB;

class APIController extends Controller
{
    public $unauthorised = 401;
    public $successStatus = 200;
    public $badrequest = 400;


    /**************************************************************************/


    public function register(Request $request){

        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'email' => 'required',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 0,'message'=>"All fields are required"]);
        }

        $checkEmail = User::where(['email'=>$request->email])->first();

        if($checkEmail){
             return response()->json(['status' => 0,'message'=>'Email already exist.']);
        }

        $checkPhone = User::where(['phone_no'=>$request->phone_no])->first();

        if($checkPhone){
             return response()->json(['status' => 0,'message'=>'Phone No already exist.']);
        }


        // if ($request->hasFile('image')) {

        //     if ($request->file('image')->isValid()) {

        //        /* $validated = $request->validate([

        //             'image' => 'mimes:jpeg,png|max:1014',

        //         ]);*/

        //         //$extension = $request->image->extension();
        //         //$image_name = time().".".$extension;
        //         //$request->image->storeAs('/images', $image_name);

        //           $image_name = time().'.'.$request->image->extension();

        //         $request->image->move(public_path('images'), $image_name);

        //     }else{
        //         $image_name ="default.png";
        //     }

        // }else{
        //     $image_name ="default.png";
        // }


        $data = array(

            'username'=>$request->username,
            'email'=>$request->email,
            'password'=>Hash::make($request->password),
            'orignal_password'=>$request->password,
            );


        $users = User::create($data);
        // $url = url('public/images');
        $user = User::where(['id'=>$users->id])->select(array('*', DB::raw("CONCAT('$url/', image) AS image")))->first();

        // $data['id'] =  $users->id;


        return response()->json(['status' => 1,'message' => 'Registered Successfully.','result' =>$user]);

    }

/**************************************************************************/

    public function login(Request $request){

        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 0,'message'=>"All fields are required"]);
        }
        // $url = url('public/images');
        $checkEmail = User::where(['email'=>$request->email])->first();
        if($checkEmail){

            if (Hash::check($request->password, $checkEmail->password))
            {
                return response()->json(['status' => 1,'message' => 'Login Successfully.','result' => $checkEmail]);
            }else{
                return response()->json(['status' => 0,'message'=>'Password Mismatched']);
            }
        }else{
            return response()->json(['status' => 0,'message'=>'Account Not Registered or Deactivated Please contact support for more information.']);
        }


    }

/**************************************************************************/


    public function changePassword(Request $request){

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'password' => 'required',
            'cpassword' => 'required',

        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 0,'message'=>"All fields are required"]);
        }

        $user = User::where(['id'=>$request->user_id])->first();

        if(!$user){
            return response()->json(['status' => 0,'message'=>'Invalid user id']);
        }

        if ($request->password  != $request->cpassword)
        {
            return response()->json(['status' => 0,'message'=>'Password Mismatch']);
        }

        User::where('id', $request->user_id)->update([
           'password' => Hash::make($request->password),
           'orignal_password' => $request->password,
        ]);

        return response()->json(['status' => 1,'message' => 'Changed Successfully.']);


    }


/**************************************************************************/


    public function forgotPassword(Request $request){

        $validator = Validator::make($request->all(), [
            // 'user_id' => 'required',
            'email' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 0,'message'=>"All fields are required"]);
        }

        $user = User::where(['email'=>$request->email])->first();

        if(!$user){
            return response()->json(['status' => 0,'message'=>'Invalid user email']);
        }


        $otp  = rand(1111,9999);

        // User::where('id', $request->user_id)->update([
        //    'otp' => $otp
        // ]);

       	$result = ['otp'=>$otp];

       return response()->json(['status' => 1,'message' => 'OTP sent to your email.','result' => $result]);


    }


/**************************************************************************/


    public function verifyOtp(Request $request){

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'otp' => 'required',

        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 0,'message'=>"All fields are required"]);
        }

        $user = User::where(['id'=>$request->user_id,'otp'=>$request->otp])->first();

        if(!$user){
            return response()->json(['status' => 0,'message'=>'Invalid OTP']);
        }

        User::where('id', $request->user_id)->update([
           'is_active' => 2
        ]);

        return response()->json(['status' => 1,'message' => 'Verified Successfully.']);

    }

/**************************************************************************/

    public function editProfile(Request $request){

         $validator = Validator::make($request->all(), [
            'user_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 0,'message'=>"All fields are required"]);
        }

        $user = User::where(['id'=>$request->user_id])->first();

        if(!$user){
            return response()->json(['status' => 0,'message'=>'Invalid user id']);
        }

        if ($request->hasFile('image')) {

            if ($request->file('image')->isValid()) {

               /* $validated = $request->validate([

                    'image' => 'mimes:jpeg,png|max:1014',

                ]);*/

                //$extension = $request->image->extension();
                //$image_name = time().".".$extension;
                //$request->image->storeAs('/images', $image_name);

                $image_name = time().'.'.$request->image->extension();

                $request->image->move(public_path('images'), $image_name);

            }else{
                $image_name = $user->image;
            }

        }else{
            $image_name = $user->image;
        }


        $data = array(
        	'user_login'=>$request->username,
        	'display_name'=>$request->username,
            'user_nicename'=>$request->username,
            'user_email'=>$request->email,
            'image'=>$image_name?$image_name:'default.jpg',
        );

         User::where('id', $request->user_id)->update($data);

        return response()->json(['status' => 1,'message' => 'Updated Successfully.']);

    }

    public function getProfile(Request $request){

         $validator = Validator::make($request->all(), [
            'user_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 0,'message'=>"All fields are required"]);
        }


        // $url = url('/images');

        // $user = User::where(['id'=>$request->user_id])->select(array('*', DB::raw("CONCAT('$url/', image) AS image")))->first();

        $user = User::where(['id'=>$request->user_id])->first();

        if(!$user){
            return response()->json(['status' => 0,'message'=>'Invalid user id']);
        }

        return response()->json(['status' => 1,'message' => 'Success','result'=>$user]);

    }

    public function getCategory(Request $request){
    	$validator = Validator::make($request->all(), [
            'user_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>$validator->errors()->first(),'status'=>'false'], $this->badrequest);
        }else{
        	$addArr = [];
            $addresses = Category::orderBy('term_id','ASC')
            ->where('slug','!=','uncategorized')
            ->where('slug','!=','main-menu')
            ->where('slug','!=','footer-menu')
            ->get();
            $larr = json_decode($addresses,true);
            foreach($larr AS $add){
            	$list['id'] = (String)$add['term_id'];
            	$list['name'] = $add['name'];
            	$addArr[] = $list;
            }
	        return response()->json([
	        	'status'=>'1',
                'message'=>'Categories fetched successfully',
                'cartArray'=>$addArr
            ], $this->successStatus);
        }
    }

    public function addPost(Request $request){

        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'category' => 'required',
            'visibility' => 'required',
            'picture' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 0,'message'=>"All fields are required"]);
        }


        if ($request->hasFile('picture')) {

            if ($request->file('picture')->isValid()) {

            	$image_name = time().'.'.$request->picture->extension();

                $request->picture->move(public_path('images'), $image_name);

            }else{
                $image_name ="default.png";
            }

        }else{
            $image_name ="default.png";
        }


        $data = array(
        	'title'=>$request->title,
            'email'=>$request->description,
            'password'=>$request->category,
            'orignal_password'=>$request->visibility,
            'orignal_password'=>$request->picture
        );


        $posts = Post::create($data);
        // $url = url('public/images');
        $post = Post::where(['id'=>$posts->id])->first();


        return response()->json(['status' => 1,'message' => 'Post Created Successfully.','result' =>$post]);

    }

    public function addAssetToTimeline(Request $request){

        $validator = Validator::make($request->all(), [
            'asset_id' => 'required',
            'title' => 'required',
            'description' => 'required',
            'status' => 'required',
            'expense' => 'required',
            'year' => 'required',
            'month' => 'required',
            'videourl' => 'required',
            'picture' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 0,'message'=>"All fields are required"]);
        }


        if ($request->hasFile('picture')) {

            if ($request->file('picture')->isValid()) {

            	$image_name = time().'.'.$request->picture->extension();

                $request->picture->move(public_path('images'), $image_name);

            }else{
                $image_name ="default.png";
            }

        }else{
            $image_name ="default.png";
        }


        $data = array(
        	'title'=>$request->title,
            'email'=>$request->description,
            'password'=>$request->category,
            'orignal_password'=>$request->visibility,
            'orignal_password'=>$request->picture
        );


        $posts = Post::create($data);
        // $url = url('public/images');
        $post = Post::where(['id'=>$posts->id])->first();


        return response()->json(['status' => 1,'message' => 'Post Created Successfully.','result' =>$post]);

    }
}
