<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login','APIController@login');
// Route::post('socialLogin','UserAPIController@socialLogin');
Route::post('register','APIController@register');
Route::post('forgotPassword','APIController@forgotPassword');
Route::post('verifyEmail','APIController@verifyEmail');
Route::post('resetPassword','APIController@changePassword');
Route::post('getOwnProfile', 'APIController@getOwnProfile');
Route::post('editProfile', 'APIController@editProfile');
Route::post('homelisting', 'APIController@homelisting');
Route::post('addAsset', 'APIController@addPost');
Route::post('editAsset', 'APIController@editPost');
Route::post('getCategory', 'APIController@getCategory');
Route::post('visibilityList', 'APIController@visibilityList');
Route::post('addAssetToTimeline', 'APIController@addAssetToTimeline');
Route::post('search', 'APIController@search');
